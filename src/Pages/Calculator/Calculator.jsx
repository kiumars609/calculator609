import React, { useState } from 'react'
import './style.css'

export default function Calculator() {
    const [val, setVal] = useState("")

    const clean = () => {
        try {
            setVal(val.slice(0, -1))
        } catch (error) {
            setVal("")
        }
    }

    const gleisch = () => {
        try {
            setVal(eval(val))
        } catch (error) {
            setVal("Error")
        }
    }

    return (
        <div className="justify-content-center mx-auto row main-calcuator">
            <div className='col-md-3'>
                <div className="card shadow">
                    <div className="card-main p-2">
                        <input type="text" className='form-control form-control-lg fs-2' value={val} onChange={(e) => setVal(e.target.value)} />

                        <div className="row">
                            <div className="col-md-6">
                                <button className="btn col-md-12 shadow fs-4" onClick={(e) => clean()}>C/CE</button>
                            </div>
                        </div>

                        <div className="row mt-2">
                            <div className="col-md-3">
                                <button className="btn col-md-12 shadow fs-4" value="1" onClick={(e) => setVal(val + e.target.value)}>1</button>
                            </div>
                            <div className="col-md-3">
                                <button className="btn col-md-12 shadow fs-4" value="2" onClick={(e) => setVal(val + e.target.value)}>2</button>
                            </div>
                            <div className="col-md-3">
                                <button className="btn col-md-12 shadow fs-4" value="3" onClick={(e) => setVal(val + e.target.value)}>3</button>
                            </div>
                            <div className="col-md-3">
                                <button className="btn col-md-12 shadow fs-4" value="+" onClick={(e) => setVal(val + e.target.value)}>+</button>
                            </div>
                        </div>

                        <div className="row mt-2">
                            <div className="col-md-3">
                                <button className="btn col-md-12 shadow fs-4" value="4" onClick={(e) => setVal(val + e.target.value)}>4</button>
                            </div>
                            <div className="col-md-3">
                                <button className="btn col-md-12 shadow fs-4" value="5" onClick={(e) => setVal(val + e.target.value)}>5</button>
                            </div>
                            <div className="col-md-3">
                                <button className="btn col-md-12 shadow fs-4" value="6" onClick={(e) => setVal(val + e.target.value)}>6</button>
                            </div>
                            <div className="col-md-3">
                                <button className="btn col-md-12 shadow fs-4" value="-" onClick={(e) => setVal(val + e.target.value)}>-</button>
                            </div>
                        </div>

                        <div className="row mt-2">
                            <div className="col-md-3">
                                <button className="btn col-md-12 shadow fs-4" value="7" onClick={(e) => setVal(val + e.target.value)}>7</button>
                            </div>
                            <div className="col-md-3">
                                <button className="btn col-md-12 shadow fs-4" value="8" onClick={(e) => setVal(val + e.target.value)}>8</button>
                            </div>
                            <div className="col-md-3">
                                <button className="btn col-md-12 shadow fs-4" value="9" onClick={(e) => setVal(val + e.target.value)}>9</button>
                            </div>
                            <div className="col-md-3">
                                <button className="btn col-md-12 shadow fs-4" value="*" onClick={(e) => setVal(val + e.target.value)}>x</button>
                            </div>
                        </div>

                        <div className="row mt-2">
                            <div className="col-md-3">
                                <button className="btn col-md-12 shadow fs-4" value="." onClick={(e) => setVal(val + e.target.value)}>.</button>
                            </div>
                            <div className="col-md-3">
                                <button className="btn col-md-12 shadow fs-4" value="0" onClick={(e) => setVal(val + e.target.value)}>0</button>
                            </div>
                            <div className="col-md-3">
                                <button className="btn col-md-12 shadow fs-4" onClick={(e) => gleisch()}>=</button>
                            </div>
                            <div className="col-md-3">
                                <button className="btn col-md-12 shadow fs-4" value="/" onClick={(e) => setVal(val + e.target.value)}>/</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
